import React, { Component } from 'react';
import './App.css';
import Product from '../Product/Product';
import products from '../../mock-data.js';
class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: products,
    }
  }
  render() {
    const products = this.state.data.map(product => {
      return (
        <div key={product.id} className="column-container">
          <Product data={product} />
        </div>
      )
    })
    return (
      <div className="app">
        <div className="container">
          <header className="app__header">
            <h1 className="app__title">Ты сегодня покормил кота?</h1>
          </header>
            {products}
        </div>
      </div>
    );
  }
}
export default App;
