import React from 'react';
import PropTypes from 'prop-types';
import './Product.css';
class Product extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            selected: false,
            hovered: false,
        }
    }
    toggleSelect = (e) => {
        e.preventDefault();
        if (this.props.data.available) {
            this.setState(prevState => ({selected: !prevState.selected, hovered: false}))
        }
    }
    handleMouseEnter = () => {
        this.setState({ hovered: true })
    }
    handleMouseLeave = () => {
        this.setState({hovered: false})
    }
    getNumberStr = (str) => {
        return str.match(/\d{1,}/);
    }
    render(){
        const {title, subtitle, subtitle2, fill, size, gift, additional_text, count, available, buy_text, buy_text_info, buy_text_none} = this.props.data;
        
        //Формируем классы
        let className = available ? 'product' : 'product product_disabled'; //присваиваем класс в зависимости от наличия товара
        className += this.state.selected & available ? ' product_selected' : '';
        className += this.state.hovered & available ? ' product_hovered' : ''; 
        //Формируем текст под блоком
        if (!available) { //Если товар недоступен
            var buyText = buy_text_none; //покажем текст о недоступности
        } else if (this.state.selected) { //если товар выделен, покажем описание товара
            buyText = buy_text_info;
        } else { //в противном случае покажем текст с предложением о покупке
            buyText = buy_text;
        }
        //Формируем ссылку под блоком
        let link;
        if (!this.state.selected && available) { //если товар не выделен и доступен, тогда добавим ссылку
            link = (
            <a onClick={this.toggleSelect} className="product__buy-link" href="/">
                <span className="product__buy-link_underline-dashed">купи</span>.
            </a>
            );
        }
        //Формируем подзаголовок в зависимости от того, выбран товар или нет
        const subTitle = this.state.hovered & this.state.selected ? subtitle2 : subtitle;

        //Извлекаем число из строки size (чтобы выделить числа полужирным начетртанием)
        let newTextSize = size; //Если условие не сработает, то выведем просто строку size 
        if (this.getNumberStr(size) !== null){ //Если условие сработает, то выделим число
            let number = this.getNumberStr(size);
            newTextSize = (
                <React.Fragment>
                    <span className="product__size_strong">{number}</span>{size.replace(number, '')}
                </React.Fragment>
            );
        }
        //Извлекаем число из строки gift (чтобы выделить число полужирным начетртанием)
        let newTextGift = gift; //Если условие не сработает, то выведем просто строку gift
        if (this.getNumberStr(gift) !== null){ //Если условие не сработает, то выведем просто строку size 
            let number = this.getNumberStr(gift);
            newTextGift = (
                <React.Fragment>
                    <span className="product__gift_strong">{number}</span>{gift.replace(number, '')}
                </React.Fragment>
            );
        }

        return(
            <React.Fragment>
            <div className={className} onClick={this.toggleSelect} onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave}>
                <p className="product__subtitle">{subTitle}</p>
                <h3 className="product__title">{title}</h3>
                <h4 className="product__filling">{fill}</h4>
                <p className="product__info">
                    <span className="product__size">{newTextSize}</span>
                    <span className="product__gift">{newTextGift}</span>
                    <span className="product__additional-text">{additional_text}</span>
                </p>
                <div className="product__weight">
                    <span className="product__count">{count}</span>
                    <span className="product__units">кг</span>
                </div>
            </div>
            <p className="product__buy-text">{buyText} {link}</p>
            </React.Fragment>
        );
    }

}

Product.propTypes = {
    data: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        subtitle: PropTypes.string.isRequired,
        subtitle2: PropTypes.string.isRequired,
        fill: PropTypes.string.isRequired,
        size: PropTypes.string.isRequired,
        gift: PropTypes.string.isRequired,
        additional_text: PropTypes.string,
        count: PropTypes.string.isRequired,
        available: PropTypes.bool.isRequired,
        buy_text: PropTypes.string.isRequired,
        buy_text_info: PropTypes.string.isRequired,
        buy_text_none: PropTypes.string.isRequired
    })
}

export default Product;