const products = [
    {
        id: 1,
        title: 'Нямушка',
        subtitle: 'Сказочное заморское яство',
        subtitle2: 'Котэ не одобряет?',
        fill: 'с фуа-гра',
        size: '10 порций',
        gift: 'мышь в подарок',
        additional_text: '',
        count: '0,5',
        available: true,
        buy_text: 'Чего сидишь? Порадуй котэ,',
        buy_text_info: 'Печень утки разварная с артишоками.',
        buy_text_none: 'Печалька, с фуа-гра закончился.'
    },
    {
        id: 2,
        title: 'Нямушка',
        subtitle: 'Сказочное заморское яство',
        subtitle2: 'Котэ не одобряет?',
        fill: 'с рыбой',
        size: '40 порций',
        gift: '2 мыши в подарок',
        additional_text: '',
        count: '2',
        available: true,
        buy_text: 'Чего сидишь? Порадуй котэ,',
        buy_text_info: 'Головы щучьи с чесноком да свежайшая сёмгушка.',
        buy_text_none: 'Печалька, с рыбой закончился.'
    },
    {
        id: 3,
        title: 'Нямушка',
        subtitle: 'Сказочное заморское яство',
        subtitle2: 'Котэ не одобряет?',
        fill: 'с курой',
        size: '100 порций',
        gift: '5 мышей в подарок',
        additional_text: 'заказчик доволен',
        count: '5',
        available: false,
        buy_text: 'Чего сидишь? Порадуй котэ,',
        buy_text_info: 'Филе из цыплят с трюфелями в бульоне.',
        buy_text_none: 'Печалька, с курой закончился.'
    },
];

export default products;